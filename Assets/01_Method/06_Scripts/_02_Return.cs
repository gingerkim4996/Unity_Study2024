using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// 20240513
// 고박사의 유니티 노트 10강 메소드
// https://youtu.be/o0Pklkow-AA

// 2. 리턴(return)

// 1) return이 호출되면 메소드를 종료시키고
//    메소드를 호출 했던 곳으로 돌아간다.

// 2) 메소드 내부에서 여러 방법에 의해 변환된 데이터를
//    특정하여 반환하는 용도로 사용.
//    반환할 데이터가 없을 경우 return만 호출하여 메소드를 종료시킬수 있다.
//    이를 이용해 원하는 타이밍에 데이터를 반환시키거나
//    메소드를 종료시키는 용도로 사용할 수 있다.

public class _02_Return : MonoBehaviour
{   
    // 2. 리턴(return)
    private void Awake()
    {
        int a = Max(10, 20);
        Debug.Log(Max(100, 200));
        Max(5, 10);
    }
    public int Max(int num1, int num2)
    {
        if (num1 > num2)
        {
            Debug.Log(num1 + " Debug3");
            return num1;
        }
        Debug.Log(num2 + " Debug4");
        return num2;
    }
}