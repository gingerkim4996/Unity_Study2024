using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// 20240513
// 고박사의 유니티 노트 10강 메소드
// https://youtu.be/o0Pklkow-AA

// 3. 매개변수(Parameter)
// 1) 메소드 내부에서 사용하는 ""지역 변수""
// 2) 메소드 외부에서 내부로 데이터 전달을 위해 사용하기도 함.


public class _03_Parameter: MonoBehaviour
{
    // 3. 매개변수(Parameter)
    // 
    void Start()
    {
    }
}
