using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// 20240430
// 고박사의 유니티 노트 10강 메소드
// https://youtu.be/o0Pklkow-AA

// 1. 메소드(Method)
// 1.1 개요
// 객체 지향 프로그래밍 언어에서 사용하는 용어
// 유사한 용어(사용하는 언어에 따라 다르게 부른다)
// 함수(Function), 서브루틴(SubRoutine), 프로시져(Procedure), 서브프로그램(SubProgram)

// 방법이라는 뜻으로 객체의 일을 처리하는 방법을 정의한다.
// 하나의 목적을 가진 코드를 하나의 이름 아래 묶은 것이다.
// 묶여진 코드들은 메소드의 이름을 호출할 시 실행할 수 있다.

// 1.2 장점
// 코드를 묶어두고 이름을 불러 실행하기 때문에
// 아래와 같이 동일한 내용의 코드를 반복해서 작성하는 수고가 적어진다.

public class _01_Method : MonoBehaviour
{
    // 1. 메소드(Method)
    void Start()
    {
        // 아래는 메소드를 이용하지 않은 방법이다.
        int num1 = 0, num2 = 0, result = 0;

        num1 = 44;
        num2 = 55;
        result = num1 * num2;
        Debug.Log($"{num1} x {num2} = {result}");

        num1 = 55;
        num2 = 66;
        result = num1 * num2;
        Debug.Log($"{num1} x {num2} = {result}");

        // 아래는 메소드를 이용한 방법이다.
        // 메소드(Multiple)와 매개변수(num1, num2)를 불러서 사용.
        Multiple(3, 4);
        Multiple(5, 8);
        Multiple(4, 6);

        // num1와 num2의 곱을 출력하는 메소드의 정의
        void Multiple(int num1, int num2)
        {
            int result = num1 * num2;
            Debug.Log($"{num1} x {num2} = {result}");
        }
    }
}
